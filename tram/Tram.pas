uses crt,graph;

var GD,GM:integer;
var TRAM_START_X_POSITION, TRAM_START_Y_POSITION:integer;

const 
TRAM_WIDTH=900;
TRAM_HEIGHT=300;

WINDOW_SIZE=80;

X_DIFFERENCE=100;
TRAM_BASE_COLOR=RED;
TRAM_WINDOW_COLOR=BLUE;
TRAM_WHEEL_COLOR=WHITE;

procedure drawTramBase();
	const POINT_SIZE=7 ; 
	var tramBase : array[0 .. POINT_SIZE] of PointType;
	begin
		setColor(TRAM_BASE_COLOR);
		tramBase[0].x:=TRAM_START_X_POSITION;
		tramBase[0].y:=TRAM_START_Y_POSITION;
		
		tramBase[1].x:=TRAM_START_X_POSITION+TRAM_WIDTH;
		tramBase[1].y:=TRAM_START_Y_POSITION;
		
		tramBase[2].x:=tramBase[1].x+X_DIFFERENCE;
		tramBase[2].y:=tramBase[1].y+(TRAM_HEIGHT div 2);
		
		tramBase[3].x:=tramBase[1].x;
		tramBase[3].y:=tramBase[2].y+(TRAM_HEIGHT div 2);
		
		tramBase[4].x:=tramBase[0].x;
		tramBase[4].y:=tramBase[3].y;
		
		tramBase[5].x:=tramBase[4].x-X_DIFFERENCE;
		tramBase[5].y:=tramBase[4].y-(TRAM_HEIGHT div 2);
		
		tramBase[6].x:=tramBase[0].x;
		tramBase[6].y:=tramBase[0].y;

		DrawPoly(POINT_SIZE, tramBase);
	end;

procedure drawWindows();
	var 
	windowsCount,
	xPosition,
	yPosition,
	
	windowsIterator:integer;
	begin
		setColor(TRAM_WINDOW_COLOR);
		windowsCount:=(TRAM_WIDTH div (WINDOW_SIZE*2))-1;
		
		xPosition:=TRAM_START_X_POSITION + (X_DIFFERENCE div 2);
		yPosition:=TRAM_START_Y_POSITION + (X_DIFFERENCE div 2);
		
		for windowsIterator:=0 to windowsCount do
			begin
				Rectangle(xPosition,yPosition,xPosition+WINDOW_SIZE,yPosition+WINDOW_SIZE);
				xPosition:=xPosition+(WINDOW_SIZE*2);
			end;
	end;

procedure drawWheels();
		var wheelRadius:integer;
		var firstCouple,twoCouple:PointType;
		
		begin
			setColor(TRAM_WHEEL_COLOR);
			//wheelRadius:=(TRAM_WIDTH*TRAM_HEIGHT) div 5;
			wheelRadius:=30;
			
			firstCouple.x:=TRAM_START_X_POSITION+wheelRadius;
			firstCouple.y:=TRAM_START_Y_POSITION+TRAM_HEIGHT;
			
			Circle(firstCouple.x,firstCouple.y,wheelRadius);
			Circle(firstCouple.x+(wheelRadius*2),firstCouple.y,wheelRadius);
			
			twoCouple.x:=(TRAM_START_X_POSITION+TRAM_WIDTH)-wheelRadius;
			twoCouple.y:=TRAM_START_Y_POSITION+TRAM_HEIGHT;
			
			Circle(twoCouple.x,twoCouple.y,wheelRadius);
			Circle(twoCouple.x-(wheelRadius*2),twoCouple.y,wheelRadius);
			
		end;
begin

    GD:=detect;
    GM:=0;
    initgraph(GD,GM,'');
	
	TRAM_START_X_POSITION:=(getmaxx div 4);
	TRAM_START_Y_POSITION:=(getmaxy div 4);
	
	drawTramBase();
	drawWindows();
	drawWheels();
	
	readln;
	closegraph;
end.
