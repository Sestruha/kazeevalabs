uses crt,graph;

const
//количество строк в матрице 
W_MATRIX_SIZE=8;

//количество столбцов матрицы
H_MATRIX_SIZE=8;

WHITE_VALUE=1;BLACK_VALUE=0;

//fix me, HARD CODE: SIZE
RECTANGLE_SIZE=100;

var chessField:array [0..W_MATRIX_SIZE , 0..H_MATRIX_SIZE] of Byte;
var GD,GM:integer;

//инициализация матрицы
procedure initChessField();
	var flag:boolean;
	var i1,i2:integer;
	begin
		flag:=true;
		for i1:=0 to W_MATRIX_SIZE do
			begin
				for i2:=0 to H_MATRIX_SIZE do
					begin
					
					if(flag) then chessField[i1,i2]:=WHITE_VALUE
					else chessField[i1,i2]:=BLACK_VALUE;
						
					flag:=not flag;
					
					end;
			end;
	end;

//отрисовка квадрата	
procedure drawRectangle(x,y:integer ; value:integer);
	var fillPattern:FillPatternType;
	begin
		GetFillPattern(fillPattern);
		
		if(value = WHITE_VALUE) then SetFillPattern(fillPattern, WHITE)
		else SetFillPattern(fillPattern, BLACK);
		
		//https://www.freepascal.org/docs-html/rtl/graph/funcfilleddrawing.html
		//https://www.freepascal.org/docs-html/rtl/graph/funcdrawing.html
		//закрашенный прямоугольник, аналог: Rectangle
		Bar(x,y,x+RECTANGLE_SIZE,y+RECTANGLE_SIZE);
	end;
	
//отрисовка шахматной доски
procedure drawField();
	var currentX,currentY,i1,i2:integer;
	
	begin
		currentX:=0;
		currentY:=0;
		
		for i1:=0 to W_MATRIX_SIZE do
			begin
				for i2:=0 to H_MATRIX_SIZE do
					begin
						drawRectangle(currentX,currentY,chessField[i1,i2]);
						currentX:=currentX+RECTANGLE_SIZE;
					end;
				currentY:=currentY+RECTANGLE_SIZE;
				currentX:=0;
			end;	
	end;
begin

    GD:=detect;
    GM:=0;
    initgraph(GD,GM,'');
	
	initChessField();
	drawField();

	readln;
	closegraph;
end.
