// crt - https://pas1.ru/crt
// graph - https://pas1.ru/graph
uses crt,graph;

type Point = record
	x:integer;
	y:integer;
end;

type Triangle = record
	a:Point;
	b:Point;
	c:Point;
end;

const COLOR=15;MAX_COLOR=21;deepRecursive=10;
 
var GD,GM:integer;
var mainTriangle:Triangle;

procedure triangleDraw(drawerTriangle:Triangle);
	begin
		moveto(drawerTriangle.a.x,drawerTriangle.a.y);
		
		setcolor(RED);
		lineto(drawerTriangle.b.x,drawerTriangle.b.y);
		
		setcolor(BLUE);
		lineto(drawerTriangle.c.x,drawerTriangle.c.y);
		
		setcolor(GREEN);
		lineto(drawerTriangle.a.x,drawerTriangle.a.y);
	end;
	
procedure initTriangle();
	begin
		mainTriangle.a.x:=0;
		mainTriangle.a.y:=0;
		
		mainTriangle.b.x:=getmaxx;
		mainTriangle.b.y:=0;
		
		mainTriangle.c.x:=0;
		mainTriangle.c.y:=getmaxy;
	end;
	
procedure recursiveTriangle(currentTriangle:Triangle;n:integer);
	const K = 2;
	
	var smallTriangle,leftTriangle,rightTriangle,bottomTriangle:Triangle;
	
	begin
		if n>0 then
			begin
			
				//smallTriangle
				smallTriangle.a.x:=(currentTriangle.a.x + currentTriangle.c.x) div K;
				smallTriangle.a.y:=(currentTriangle.a.y + currentTriangle.c.y) div K;
				
				smallTriangle.b.x:=(currentTriangle.b.x + currentTriangle.a.x) div K;
				smallTriangle.b.y:=(currentTriangle.b.y + currentTriangle.a.y) div K;
				
				smallTriangle.c.x:=(currentTriangle.b.x + currentTriangle.c.x) div K;
				smallTriangle.c.y:=(currentTriangle.b.y + currentTriangle.c.y) div K;
				
				//leftTriangle
				leftTriangle.a.x:=currentTriangle.a.x;
				leftTriangle.a.y:=currentTriangle.a.y;
				
				leftTriangle.b.x:=smallTriangle.b.x;
				leftTriangle.b.y:=smallTriangle.b.y;
				
				leftTriangle.c.x:=smallTriangle.a.x;
				leftTriangle.c.y:=smallTriangle.a.y;
				
				//rightTriangle
				rightTriangle.a.x:=smallTriangle.b.x;
				rightTriangle.a.y:=smallTriangle.b.y;
				
				rightTriangle.b.x:=currentTriangle.b.x;
				rightTriangle.b.y:=currentTriangle.b.y;
				
				rightTriangle.c.x:=smallTriangle.c.x;
				rightTriangle.c.y:=smallTriangle.c.y;
				
				//bottomTriangle
				bottomTriangle.a.x:=currentTriangle.c.x;
				bottomTriangle.a.y:=currentTriangle.c.y;
				
				bottomTriangle.b.x:=smallTriangle.a.x;
				bottomTriangle.b.y:=smallTriangle.a.y;
				
				bottomTriangle.c.x:=smallTriangle.c.x;
				bottomTriangle.c.y:=smallTriangle.c.y;
				

				triangleDraw(leftTriangle);
				triangleDraw(rightTriangle);
				triangleDraw(bottomTriangle);
				//triangleDraw(smallTriangle);
				
				recursiveTriangle(leftTriangle,n-1);
				recursiveTriangle(rightTriangle,n-1);
				recursiveTriangle(bottomTriangle,n-1);
				//recursiveTriangle(smallTriangle,n-1);
			end;
	end;
	
begin

    GD:=detect;
    GM:=0;
    initgraph(GD,GM,'');
	
	setcolor(COLOR);
	initTriangle();
	triangleDraw(mainTriangle);

	recursiveTriangle(mainTriangle,deepRecursive);
	
	readln;
	closegraph;
end.
